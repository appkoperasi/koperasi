<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	
	public function index() {
		echo "Halo dunia";
	}
	
	public function cek_user() {
		$this->load->database();
		$uid = $this->input->post("uid");
		$query = $this->db->get_where("users", array(
			"google_id" => $uid
		))->result();
		if ($query->num_rows() > 0) {
			echo 1;
		} else {
			echo 0;
		}
	}
	
	public function login() {
		$phone = utf8_decode(urldecode($this->input->post("phone")));
		$password = $this->input->post("password");
		$query = $this->db->get_where("users", array(
			"phone" => $phone
		));
		if ($query->num_rows() > 0) {
			$user = $query->row_array();
			if (password_verify($password, $user["password"])) {
				$response = array(
					"id" => intval($user["id"]),
					"response" => 1
				);
				echo json_encode($response);
			} else {
				$response = array(
					"id" => 0,
					"response" => -2
				);
				echo json_encode($response);
			}
		} else {
			// Phone not exists
			$response = array(
				"id" => 0,
				"response" => -1
			);
			echo json_encode($response);
		}
	}
}
